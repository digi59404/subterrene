using Gtk;
using Vte;

namespace Subterrene {

    class TerminalWidget : Vte.Terminal {
	GLib.Pid child_pid;
	
	public TerminalWidget(){
	    base.fork_command_full(Vte.PtyFlags.DEFAULT, "/home", { Vte.get_user_shell () },null, SpawnFlags.SEARCH_PATH, null, out this.child_pid);

	}

    }
}
