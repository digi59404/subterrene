using Gtk;
using Granite;

namespace Subterrene {

    class WelcomeView : Gtk.Box{
    
        public MainWindow main_window;

        public Granite.Widgets.Welcome lbl;

        construct {

        }

        public WelcomeView(MainWindow window){

            main_window = window;
            //Build the welcome screen!
            lbl = new Granite.Widgets.Welcome("Subterrene", "Connecting You To The World! One console at a time..");
            lbl.append("system-log-out", "Connect To Somewhere", "Lets Travel The World!");
            lbl.append("document-properties", "Manage Servers", "Where are we going today?");
            lbl.append("help-info", "About Subterrene", "Extra! Extra! Read all aboot it.");
            
            //Lets connect the welcome menu to a function!
            lbl.activated.connect (on_activated);        

            base.pack_start(lbl);
            
            //return lbl;
        }
        private void on_activated(int index) {

            switch (index) {
                case 0: // New 
		    main_window.set_content(main_window.sv);
                break;

                case 1: // Manage 

                break;

                case 2: // Go Back
		    main_window.show_about();
                break;
            }
        }
    }
}
