using Gtk;
using Granite;

namespace Subterrene {


    class ServerView : Granite.Widgets.ThinPaned {

	public Granite.Widgets.SourceList sidebar;
        public Granite.Widgets.SourceList.ExpandableItem serverlist;
        public MainWindow main_w;
       
            
	public ServerView(MainWindow main_w){
	    this.main_w = main_w;
	    /*
	     * Make the Sourcelist.
	     */

	    sidebar = new Granite.Widgets.SourceList ();
            sidebar.set_sort_func ((a, b) => {
		if (a is Granite.Widgets.SourceList.ExpandableItem && b is Granite.Widgets.SourceList.ExpandableItem)
		    return a.name.collate (b.name);
		if (a is Granite.Widgets.SourceList.ExpandableItem)
		    return -1;
		if (b is Granite.Widgets.SourceList.ExpandableItem)
		    return 1;

		return a.name.collate(b.name);
	    });
            

            serverlist = new Granite.Widgets.SourceList.ExpandableItem ("Fav SSH Servers");
            var dummyserver = new SSH_Location ("Server #1", main_w);
            serverlist.add(dummyserver);

            // Add and expand categories
            sidebar.root.add (serverlist);
            sidebar.root.expand_all ();


	    base.set_position(175);
	    base.pack1(sidebar, true, true);
	    var lbl = new WelcomeView(main_w);
	    base.pack2(lbl, true, true);
	}
	
	public void setTerminal(Widget wi){
	    //var ch2 = ;
	    base.remove(base.get_child2());
	    
	    base.pack2(wi, true, true);
	    base.show_all();
	}
    
    }
}
