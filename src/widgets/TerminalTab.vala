using Gtk;
using Vte;

namespace Subterrene {

    class TerminalTab : Gtk.Box{
	public Granite.Widgets.DynamicNotebook notebook;
	public string server_name;
        public string username;
	
	public TerminalTab(string server_name1, string username1){
	    this.server_name = server_name1;
	    this.username = username1;
	
	    notebook = new Granite.Widgets.DynamicNotebook ();
            notebook.show_icons = false;
           // notebook.tab_switched.connect (on_switch_page);
            //notebook.tab_moved.connect (on_tab_moved);
            notebook.allow_new_window = true;
            notebook.allow_duplication = false;
           // notebook.margin_top = 3;

            notebook.tab_added.connect ((tab) => {
                new_tab ("", tab);
            });

            notebook.tab_removed.connect ((tab) => {
                return true;
            });
            
            new_tab("", null);
            base.pack_start(notebook, true, true, 0);
	}
	
	public void new_tab(string location, owned Granite.Widgets.Tab? tab=null){
	    var terminal = new TerminalWidget();
	    
	    if(this.server_name != null)
		terminal.feed_child("ssh "+username+"@"+server_name+"\n",-1);
	    
	    bool insert_tab = false;
	    if(tab == null){
		tab = new Granite.Widgets.Tab("SSH Terminal", null, terminal);
		insert_tab = true;
	    } else {
		tab.page = terminal;
		tab.label = "SSH Terminal";
		tab.page.show_all();
	    }
	    
	    if (insert_tab)
                notebook.insert_tab (tab, -1);

            notebook.current = tab;
	}
    }
}
