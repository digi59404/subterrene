using Gtk;
using Granite;

namespace Subterrene {

    
    class MainWindow : Gtk.Window {

        /* APPLICATION INFORMATION */
        const string program_name = "Subterrene";
        const string exec_name = "Subterrene";

        const string app_years = "2012-2013";
        const string app_version = "0.1";
        const string application_id = "com.elementary.Subterrene";
        const string app_icon = "./assets/eStudio.svg";
        const string app_launcher = "Subterrene.desktop";
        const string app_website = "http://ChristopherTimberlake.com";
        const string app_website_lbl = "Website";
        
        const string[] app_authors = {"Chris Timberlake <game64@gmail.com>"};
        const string[] app_documenters = {"Chris Timberlake <game64@gmail.com>"};
        const string[] app_translators = {"Chris Timberlake <game64@gmail.com>"};
        const string[] app_artists = {"Chris Timberlake <game64@gmail.com>"};

        const string app_license_name = "MIT License";
        const Gtk.License app_license = License.MIT_X11;
        
        const string app_comments = "NONE";

    
        //Define variables.
        Gtk.Box                 main_container;
        Gtk.Toolbar             main_toolbar;   
        Gtk.ToolItem            main_title;
        public string		current_widget;
        public ServerView      sv;

        string maximized 	= "false";

        const int icon_size	 = Gtk.IconSize.LARGE_TOOLBAR; 
        const int icon_height   = 48;
        
        const string main_toolbar_css      = """
                                    .title {
                                            color: #666;
                                            text-shadow: 0px 1px 0px white;
                                    }
                                    .toolbar {
                                            padding: 0px;
                                            box-shadow: inset 0px 1px 0px rgba(255,255,255,0.3);
                                    }
                                  """;
                             
        Gtk.CssProvider css; 
        /* CUSTOM WIDGETS */
        
        // It's just good coding practice in any language to define everything before it's used.
        // Even if that specific language doesn't care.
        public Gtk.ToolItem toolbar_seperator() {
    
            //Create the Seperator Referance
            var sep = new Gtk.ToolItem ();
            //Set Height/Width
            sep.height_request = icon_height;
            sep.width_request = 1;
            
            //Design the look!
            sep.draw.connect ((cr) => {
                   cr.move_to (0, 0);
                   cr.line_to (0, 60);
                   cr.set_line_width (1);
                   var grad = new Cairo.Pattern.linear (0, 0, 0, icon_height);
                   grad.add_color_stop_rgba (0, 0.3, 0.3, 0.3, 0.4);
                   grad.add_color_stop_rgba (0.8, 0, 0, 0, 0);
                   cr.set_source (grad);
                   cr.stroke ();
                   return true;
            });
            //Add CSS Class Contexts
            sep.get_style_context ().add_class ("sep");
            sep.get_style_context ().add_provider (css, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
 
            return sep;
        }        

        //Main window's title
        Gtk.Label mw_title;
        public new string title {
            get {
                return mw_title.label;
            }
            set {
                mw_title.label = value;
            }
        }

        public MainWindow(){
            
            /* CREATE THE BASE! */            
            this.window_position = Gtk.WindowPosition.CENTER;
            var keyGroup = new Gtk.AccelGroup();
            this.add_accel_group (keyGroup);
            //Build Main Toolbar
            main_toolbar = new Gtk.Toolbar();
            //Setup the Toolbar's Look
            main_toolbar.icon_size = icon_size;

            //Build the Box Container!
            main_container = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
            main_container.pack_start (main_toolbar, false);


            /* TOOLBAR ITEMS! */
            // Close Button
            var tb_close = new Gtk.ToolButton (new Gtk.Image.from_file("/usr/share/themes/elementary/metacity-1/close.svg"), "Close");
            // Clicked Action
            tb_close.clicked.connect (() => destroy ());
            // Set the proper dimension so it doesn't look ugly!
            tb_close.height_request = icon_height;
            tb_close.width_request  = icon_height;


            // New File Button!
            var nf_menu = new Gtk.ToolButton(new Gtk.Image.from_icon_name ("list-add", Gtk.IconSize.LARGE_TOOLBAR), "Add");
            // Maximize Button
            var tb_maximize = new Gtk.ToolButton (new Gtk.Image.from_file("/usr/share/themes/elementary/metacity-1/maximize.svg"), "Close");
            // Clicked Action => TOGGLE MAXIMIZE!
            tb_maximize.clicked.connect (() => { this.toggle_maximize();  });
            // Set the proper dimension so it doesn't look ugly!
            tb_maximize.height_request = icon_height;
            tb_maximize.width_request  = icon_height;
            
            // Prepare the Main Window Title.
            mw_title = new Gtk.Label(program_name);
            // Make it look right.
            mw_title.override_font(Pango.FontDescription.from_string("bold"));
            //  Create the ToolBar Item
            main_title = new Gtk.ToolItem();
            // Add the Main Window Title.
            main_title.add(mw_title);
            // Expand it!
            main_title.set_expand(true);

            // Settings Menu!
            var settings_menu = new Gtk.Menu();
            //Make a spot for an about dlg
            Gtk.MenuItem item_about = new Gtk.MenuItem.with_label ("About");
            item_about.activate.connect (() => {
                this.show_about();

            });

            settings_menu.add(item_about);
                
            var tb_menu = new Granite.Widgets.ToolButtonWithMenu(new Gtk.Image.from_icon_name ("application-menu", Gtk.IconSize.LARGE_TOOLBAR), "", settings_menu);

            main_toolbar.insert (tb_close, -1);
            main_toolbar.insert (toolbar_seperator (), -1);
            main_toolbar.insert (nf_menu, -1);
            main_toolbar.insert (toolbar_seperator (), -1);
            main_toolbar.insert (main_title, -1);
            main_toolbar.insert (toolbar_seperator (), -1);
            main_toolbar.insert (tb_menu, -1);
            main_toolbar.insert (toolbar_seperator (), -1);
            main_toolbar.insert (tb_maximize, -1);


	    this.sv = new ServerView(this);
            //Add the Container to the Base GTK
            base.add(main_container);
            //set the default size
            base.set_default_size(800, 600);
        }
        
        // This is to check and see if it's maximized. If so, Minimize.
        public void toggle_maximize(){
            if(this.maximized == "false"){
                this.maximize ();
                this.maximized = "true";
            } else {
                this.unmaximize ();
                this.maximized = "false" ; 
            }
        }

        public void show_about(){
                            Granite.Widgets.show_about_dialog (base, 
                                               "program_name", program_name,
                                            //   "version", build_version,
                                               "logo_icon_name", app_icon,

                                               "comments", app_comments,
                                               "copyright", "%s %s Developers".printf (app_years, program_name),
                                               "website", app_website,
                                               "website_label", "Website",

                                               "authors", app_authors,
                                               "documenters", app_documenters,
                                               "artists", app_artists,
                                               "translator_credits", app_translators,
                                               "license", app_license_name,
                                               "license_type", app_license,

                                               "help", app_website,
                                               "translate", app_website,
                                               "bug", app_website);
        }

        // We need to hijack the show function of the GTK Window.
        public override void show(){
            //Show the GTK Window
            base.show();
            //Change the window style. Border-Only.
            get_window ().set_decorations (Gdk.WMDecoration.BORDER);
        }

        // We're going to use this function to add widgets to and from the main menu!
        public override void add (Gtk.Widget widget){
            main_container.pack_start(widget, true, true, 0);
        }
        
        // We're going to use this to remove widgets to and from the main menu.
        public override void remove (Gtk.Widget widget){
            main_container.remove(widget);
        }
        
        public void set_content (Gtk.Widget widget){
            if(this.current_widget != widget.name) {
                this.empty();     
                this.add(widget);
    		this.current_widget = widget.name;              
            }

            this.show_all();        
        }

        public void empty (){
            var clist = main_container.get_children();
            clist.foreach((entry) => {
                if(entry.name != "GtkToolbar"){
        	    if(entry.name == "eStudioDocView"){
        		this.main_container.remove(entry);
        	    } else {
            		entry.destroy();
            	    }
                }                 
            });
        }
    }
}



// Now that our application and window are defined, we have to call them through the main loop.
// We do this by starting with a main function. You could theoretically include all the code above
// in a main function. However it doesn't make for clean code and you wont be able to take part
// in all of granite's cool features.

// All main loop functions should start as an int. You can do self-checks within the app and 
// return 0 to force the app to close in the event of an issue.
public static int main(string[] args){

    //We need to tell GTK To start. Even though we've written all the code above. None of it
    //is started yet. So we need to start GTK then run our code.
    Gtk.init(ref args);

    //Lets assign our Application Class and Namespace to a variable. You always call it by
    // var VarName = new NameSpace.Class();
    //The Classname and Namespace Name do not have to be identical. They can be different.
    var Subterrene_MW = new Subterrene.MainWindow();
    
    Subterrene_MW.set_content(Subterrene_MW.sv);
    
    Subterrene_MW.show_all();
    Subterrene_MW.destroy.connect (Gtk.main_quit);

    //Now remember how this is an Int? Instead of returning a number we're going to return our window.
    Gtk.main();

    return 0;
}
